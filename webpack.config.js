const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { join } = require('path');

/**
 * @type {import('webpack').Configuration}
 */
const config = {
  entry: './src/index.ts',
  output: {
    filename: 'index.js',
    path: join(__dirname, 'dist', 'client'),
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.jsx']
  },
  optimization: {
    minimize: false
  },
  mode: 'production',
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.tsx?/,
        use: 'ts-loader'
      },
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader'
        ]
      }
    ]
  },
  plugins: [
    new MiniCssExtractPlugin()
  ],
  externals: {
    react: 'React',
    'react-dom': 'ReactDOM'
  }
};

module.exports = config;
