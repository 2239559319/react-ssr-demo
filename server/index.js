const Koa = require('koa');
const { compile } = require('handlebars');
const { renderToString } = require('xiaochuan-react-develop/dist/18.2.0/react-dom-server-legacy.node.production.min.js');
const { readFile } = require('fs/promises');
const { join } = require('path');
const appElem = require('../dist/server/App').default;

const app = new Koa();
const port = 9000;

const clientPath = join(__dirname, '../dist/client');

app.use(async(ctx) => {
  switch (ctx.url) {
    case '/':
    case '/index.html': {
      const template = compile(await readFile(join(__dirname, 'index.hbs'), 'utf-8'));

      const html = renderToString(appElem);
      const res = template({ content: html });

      ctx.body = res;
      ctx.type = 'html';

      break;
    }
    case '/main.css': {
      const css = await readFile(join(clientPath, 'main.css'), 'utf-8');
      ctx.body = css;
      ctx.type = 'css';
      break;
    }
    case '/index.js': {
      const js = await readFile(join(clientPath, 'index.js'), 'utf-8');
      ctx.body = js;
      ctx.type = 'js';
      break;
    }
    case '/index.js.map': {
      const js = await readFile(join(clientPath, 'index.js.map'), 'utf-8');
      ctx.body = js;
      ctx.type = 'js';
      break;
    }
    default:
      break;
  }
});

app.listen(port, () => {
  console.log(`server in port ${port}`);
});
