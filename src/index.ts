import { hydrate } from 'react-dom';
import appElem from './App';
import './index.css';

hydrate(appElem, document.querySelector('#app'));
